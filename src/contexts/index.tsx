import React, { createContext, ReactNode } from 'react';
import { RootStore } from '../stores';

export const StoreContext = createContext<RootStore>({} as RootStore);

export interface StoreComponent {
  store: RootStore;
  children: ReactNode;
}

export const StoreProvider: React.FC<StoreComponent> = ({
  children,
  store,
}) => <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
