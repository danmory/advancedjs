import styled from '@emotion/styled';

// }
// @font-face {
//     font-family: "Inter";
//     src: local("Inter-Bold"),
//     url("../../assets/Inter/Inter-VariableFont.ttf") format("truetype");
//     font-weight: normal;
// }

export const GeneralText = styled.h2`
  text-align: center;
  z-index: 1;
`;


export const GeneralContainer = styled.main`
  color: white;
  font-family: 'Inter', sans-serif;
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

export const GeneralButtonContainer = styled.div`
  color: white;
  justify-content: center;
  display: flex;
`;
