import React from 'react';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import settings from '../../settings';
import '../../assets/Inter/Inter-VariableFont.ttf';
import {
  GeneralButtonContainer,
  GeneralContainer,
  GeneralText,
} from './General.styles';

const General = () => {
  const navigate = useNavigate();

  return (
    <GeneralContainer>
      <GeneralText>Track historical changes of cryptocurrencies.</GeneralText>
      <GeneralText>
        Fast and free. Based on Coingecko and Rapid APIs.
      </GeneralText>
      <GeneralButtonContainer>
        <Button
          color="inherit"
          variant="text"
          onClick={() => navigate(settings.routes.selectCrypto)}
        >
          Select crypto ›{' '}
        </Button>
      </GeneralButtonContainer>
    </GeneralContainer>
  );
};

export default General;
