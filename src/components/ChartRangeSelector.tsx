import styled from '@emotion/styled';
import React from 'react';
import { ChartXRange } from '../types';

interface ChartRangeSelectorProps {
  handleChange: (value: ChartXRange) => unknown;
  currentValue: ChartXRange;
}

const StyledChartRangeSelector = styled.div`
  color: #ff0ee7;
  display: inline-flex;
  justify-content: space-between;
`;

const ChartRangeSelectorOption = styled.span<{ selected: boolean }>`
  cursor: pointer;
  ${({ selected }) => selected && 'text-decoration: underline;'}
  margin: 0 5px;
`;

const ChartRangeSelector: React.FC<ChartRangeSelectorProps> = ({
  handleChange,
  currentValue,
}) => {
  const options: ChartXRange[] = ['DAY', 'WEEK', 'YEAR'];
  return (
    <StyledChartRangeSelector>
      {options.map((o, idx) => (
        <ChartRangeSelectorOption
          tabIndex={0}
          onClick={() => handleChange(o)}
          selected={currentValue === o}
          key={o}
        >
          {`${o}  ${idx === options.length - 1 ? '' : '|'}`}
        </ChartRangeSelectorOption>
      ))}
    </StyledChartRangeSelector>
  );
};

export default ChartRangeSelector;
