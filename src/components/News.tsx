import styled from '@emotion/styled';
import { List, ListItem, ListItemText } from '@mui/material';
import React from 'react';
import { CoinNews } from '../types';

interface NewsProps {
  news: CoinNews[];
}

const NewsWrapper = styled.section`
  max-width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
`;

const News: React.FC<NewsProps> = ({ news }) => (
  <NewsWrapper>
    <h2>Latest news</h2>
    <List dense>
      {news.map((n) => (
        <ListItem>
          <ListItemText primary={`${n.last_updated}:  ${n.HEADLINE}`} />
        </ListItem>
      ))}
    </List>
  </NewsWrapper>
);

export default News;
