import React from 'react';
import { Cryptocurrency } from '../../types';
import bitcoinIcon from '../../assets/bitcoin_logo.png';
import ethIcon from '../../assets/eth_logo.png';
import usdtIcon from '../../assets/usdt_logo.png';
import {
  CryptoSelectButtonContainer,
  CryptoSelectButtonImage,
  CryptoSelectButtonText,
  StyledCryptoSelectButton,
} from './CryptoSelectButton.styles';

interface CryptoButtonProps {
  cryptoName: Cryptocurrency;
  onCryptoSelect: (cryptoName: Cryptocurrency) => unknown;
}

const getImageFromName = (cryptoName: Cryptocurrency) => {
  switch (cryptoName) {
    case 'BTC':
      return bitcoinIcon;
    case 'ETH':
      return ethIcon;
    case 'USDT':
      return usdtIcon;
    default:
      return null;
  }
};

const CryptoButton: React.FC<CryptoButtonProps> = ({
  cryptoName,
  onCryptoSelect,
}) => (
  <CryptoSelectButtonContainer>
    <StyledCryptoSelectButton onClick={() => onCryptoSelect(cryptoName)}>
      <CryptoSelectButtonImage
        src={getImageFromName(cryptoName)}
        alt={cryptoName}
      />
      <CryptoSelectButtonText>{cryptoName}</CryptoSelectButtonText>
    </StyledCryptoSelectButton>
  </CryptoSelectButtonContainer>
);

export default CryptoButton;
