import styled from '@emotion/styled';

export const CryptoSelectButtonContainer = styled.div`
  background: #161b22;
  border-radius: 15px;
  width: 300px;
`;

export const StyledCryptoSelectButton = styled.button`
  background: Transparent;
  border: none;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

export const CryptoSelectButtonImage = styled.img`
  position: relative;
  left: 0;
  width: 40px;
  height: 40px;
`;

export const CryptoSelectButtonText = styled.p`
  color: white;
  font-family: 'Inter', sans-serif;
  left: -1rem;
  position: relative;
  width: 100%;
`;
