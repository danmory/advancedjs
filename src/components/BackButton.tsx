import styled from '@emotion/styled';
import { Button } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import settings from '../settings';

const BackButton = () => {
  const navigate = useNavigate();

  return (
    <Button
      color="inherit"
      id="back-btn"
      variant="text"
      style={{ color: 'white' }}
      onClick={() => navigate(settings.routes.selectCrypto)}
    >
      Select crypto ›{' '}
    </Button>
  );
};

export default BackButton;
