import {
  ListItem,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  List,
  ThemeProvider,
} from '@mui/material';
import React, { useState } from 'react';
import { createTheme } from '@mui/material/styles';
import { Cryptocurrency } from '../types';
import CryptoSelectButton from './CryptoSelectButton/CryptoSelectButton';

interface SelectCryptoModalProps {
  handleClose: () => unknown;
  handleCryptoSelect: (cryptocurrency: Cryptocurrency) => unknown;
}

const dialogTheme = createTheme({
  palette: {
    primary: {
      main: '#9c27b0',
    },
    text: {
      primary: '#ffffff',
      secondary: '#bbbaba',
    },
    background: {
      paper: 'rgb(40, 44, 56)',
    },
  },
});

const SelectCryptoModal: React.FC<SelectCryptoModalProps> = ({
  handleClose,
  handleCryptoSelect,
}) => {
  const [pattern, setPattern] = useState<string>('');

  const filterCrypto = (e: React.ChangeEvent<{ value: string }>) => {
    setPattern(e.target.value);
  };

  const constructList = (pattern: string) => {
    const allCrypto: Cryptocurrency[] = ['BTC', 'ETH', 'USDT'];
    const matchedCrypto = [];
    for (let i = 0; i < allCrypto.length; i++) {
      if (allCrypto[i].toLowerCase().includes(pattern.toLowerCase())) {
        matchedCrypto.push(
          <ListItem alignItems="flex-start" key={allCrypto[i]}>
            <CryptoSelectButton
              cryptoName={allCrypto[i]}
              onCryptoSelect={handleCryptoSelect}
            />
          </ListItem>
        );
      }
    }
    return (
      <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {matchedCrypto}
      </List>
    );
  };

  return (
    <ThemeProvider theme={dialogTheme}>
      <Dialog className="mydialog" open onClose={handleClose}>
        <DialogTitle className="cryptotext">Select cryptocurrency</DialogTitle>
        <DialogContent>
          <DialogContentText className="cryptotext">
            Select cryptocurrency from available list
          </DialogContentText>
          <TextField
            fullWidth
            autoFocus
            margin="normal"
            id="cname"
            label="Cryptocurrency"
            variant="standard"
            onChange={filterCrypto}
          />
          {constructList(pattern)}
        </DialogContent>
      </Dialog>
    </ThemeProvider>
  );
};

export default SelectCryptoModal;
