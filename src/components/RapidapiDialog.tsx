import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Button,
} from '@mui/material';
import React, { useState } from 'react';

interface RapidapiDialogProps {
  open: boolean;
  handleClose: () => unknown;
  handleSubmit: (apiKey: string) => unknown;
}

const RapidapiDialog: React.FC<RapidapiDialogProps> = ({
  open,
  handleClose,
  handleSubmit,
}) => {
  const [apiKey, setApiKey] = useState<string>('');
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Getting news</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To get news, please register on rapidapi.com and enter your API key
          here.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="apikey"
          label="API key"
          type="password"
          fullWidth
          variant="standard"
          value={apiKey}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            setApiKey(event.target.value)
          }
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={() => handleSubmit(apiKey)}>Submit</Button>
      </DialogActions>
    </Dialog>
  );
};

export default RapidapiDialog;
