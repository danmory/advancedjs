import React from 'react';
import { useNavigate } from 'react-router-dom';
import settings from '../settings';
import logo from '../assets/logo.png';

const Logo = () => {
  const navigate = useNavigate();

  return (
    <div
      role="button"
      tabIndex={0}
      onClick={() => navigate(settings.routes.root)}
      style={{ cursor: 'pointer', height: '100%', zIndex: 2 }}
    >
      <img src={logo} width="199.33" height="105.33" alt="Logo" />
    </div>
  );
};

export default Logo;
