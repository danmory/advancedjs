import { Alert, Dialog } from '@mui/material';
import React from 'react';

const LoadingAlert: React.FC = ({ children }) => (
  <Dialog open>
    <Alert severity="info">{children}</Alert>
  </Dialog>
);

export default LoadingAlert;
