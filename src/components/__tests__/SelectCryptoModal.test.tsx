import { render } from '@testing-library/react';
import React from 'react';
import SelectCryptoModal from '../SelectCryptoModal';

jest.mock(
  '../../assets/bitcoin_logo.png',
  () => '../../assets/bitcoin_logo.png'
);

jest.mock('../../assets/eth_logo.png', () => '../../assets/eth_logo.png');

jest.mock('../../assets/usdt_logo.png', () => '../../assets/usdt_logo.png');

describe('SelectCryptoModal', () => {
  it('should renders correctly', () => {
    let isClicked = false;
    const page = render(
      <SelectCryptoModal
        handleClose={() => {}}
        handleCryptoSelect={() => {
          isClicked = true;
        }}
      />
    );

    const buttons = page.baseElement.getElementsByTagName('button');
    buttons[0].click();
    expect(buttons.length).toBe(3);
    expect(isClicked).toBe(true);
  });
});
