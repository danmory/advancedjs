import { render } from '@testing-library/react';
import React from 'react';
import RapidapiDialog from '../RapidapiDialog';

describe('RapidapiDialog', () => {
  it('Should renders without crashing', () => {
    render(
      <RapidapiDialog
        open
        handleClose={() => null}
        handleSubmit={(_) => null}
      />
    );
  });
});
