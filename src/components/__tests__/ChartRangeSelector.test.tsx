import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import ChartRangeSelector from '../ChartRangeSelector';

describe('ChartRangeSelector', () => {
  it('Should renders without crashing', () => {
    render(<ChartRangeSelector handleChange={() => null} currentValue="DAY" />);
  });

  it('Should react on click', () => {
    window.alert = jest.fn();
    const el = render(
      <ChartRangeSelector handleChange={(o) => alert(o)} currentValue="DAY" />
    );
    const btn = el.container
      .querySelector('div')
      .querySelector('span:nth-of-type(2)');
    fireEvent.click(btn);
    expect(window.alert).toBeCalledWith('WEEK');
  });
});
