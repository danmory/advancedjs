import { render } from '@testing-library/react';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { BrowserRouter as Router } from 'react-router-dom';
import Logo from '../Logo';
import Header from '../Header';

configure({ adapter: new Adapter() });

jest.mock('../../assets/logo.png', () => '../../assets/logo.png');

describe('Header', () => {
  it('renders without crashing', () => {
    const wrapper = mount(
      <Router>
        <Header />
      </Router>
    );
    render(wrapper);
  });
  it('clicks the logo without crashing', () => {
    const wrapper = shallow(
      <Router>
        <Logo />
      </Router>
    );
    wrapper.simulate('click');
  });
});
