import { render } from '@testing-library/react';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Background from '../Background';

configure({ adapter: new Adapter() });

describe('Background', () => {
  const wrapper = mount(<Background />);
  it('renders without crashing', () => {
    render(wrapper);
  });
});
