import { act, render, fireEvent } from '@testing-library/react';
import React from 'react';
import AnimatedCurrency from '../SelectCryptoAnimatedCurrency';

jest.mock(
  '../../assets/bitcoin_logo.png',
  () => '../../assets/bitcoin_logo.png'
);

jest.mock('../../assets/eth_logo.png', () => '../../assets/eth_logo.png');

jest.mock('../../assets/usdt_logo.png', () => '../../assets/usdt_logo.png');

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

jest.setTimeout(15000);
describe('Animated Currency', () => {
  let wrapper = null;
  let element = null;
  beforeAll(async () => {
    await act(async () => {
      wrapper = render(<AnimatedCurrency />);
      element = wrapper.container.querySelector('img');
    });
  });

  it('Make sure that images will change', async () => {
    await act(async () => {
      const initialSrc = element.src;
      await sleep(2500);
      const secondSrc = element.src;
      await sleep(2500);
      const thirdSrc = element.src;
      await sleep(2500);
      expect(initialSrc).not.toBe(secondSrc);
      expect(initialSrc).not.toBe(thirdSrc);
      expect(secondSrc).not.toBe(thirdSrc);
    });
  });

  it('Make sure that on mouseOver animation will stop', async () => {
    await act(async () => {
      const freezed = element.src;
      fireEvent.mouseOver(element);
      await sleep(2500);
      expect(element.src).toBe(freezed);
      fireEvent.mouseOut(element);
    });
  });
});
