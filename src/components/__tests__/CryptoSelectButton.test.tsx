import { render } from '@testing-library/react';
import React from 'react';
import CryptoButton from '../CryptoSelectButton/CryptoSelectButton';
import { Cryptocurrency } from '../../types';

jest.mock(
  '../../assets/bitcoin_logo.png',
  () => '../../assets/bitcoin_logo.png'
);

jest.mock('../../assets/eth_logo.png', () => '../../assets/eth_logo.png');

jest.mock('../../assets/usdt_logo.png', () => '../../assets/usdt_logo.png');

describe('CryptoSelectButton', () => {
  it('Should render crypto button', () => {
    const allCrypto: Cryptocurrency[] = ['BTC', 'ETH', 'USDT'];
    const selectedCrypto = allCrypto[0];
    const page = render(
      <CryptoButton cryptoName={selectedCrypto} onCryptoSelect={() => null} />
    );
    expect(page.container.getElementsByTagName('img')[0].alt).toBe(
      selectedCrypto
    );
  });
});
