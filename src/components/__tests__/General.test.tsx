import { render } from '@testing-library/react';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import General from '../General/General';

configure({ adapter: new Adapter() });

describe('General', () => {
  const wrapper = mount(
    <Router>
      <General />
    </Router>
  );
  it('renders without crashing', () => {
    render(wrapper);
  });
  it('clicks the button without crashing', () => {
    wrapper.find('button').simulate('click');
  });
});
