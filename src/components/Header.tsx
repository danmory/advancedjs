import styled from '@emotion/styled';
import React from 'react';
import Logo from './Logo';

const StyledHeader = styled.header`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Header: React.FC = ({ children }) => (
  <StyledHeader>
    <Logo />
    {!!children && <div>{children}</div>}
  </StyledHeader>
);

export default Header;
