import styled from '@emotion/styled';
import React, { useState } from 'react';
import bitcoin from '../assets/bitcoin_logo.png';
import eth from '../assets/eth_logo.png';
import usdt from '../assets/usdt_logo.png';

const CryptoImageDiv = styled.img`
  width: 60px;
  height: 60px;
  margin-bottom: 15px;
`;

const AnimatedCurrency = () => {
  const [imageNumber, setImageNumber] = useState(0);
  const [hovered, setHovered] = useState(false);
  let timer = null;

  const getImageStringByNumber = (num) => {
    let imageString = bitcoin;
    if (num === 0) {
      imageString = eth;
    } else if (num === 1) {
      imageString = usdt;
    }
    return imageString;
  };

  const changeImage = () => {
    if (hovered === false) {
      setImageNumber((prevState) => (prevState === 2 ? 0 : prevState + 1));
    }
  };

  React.useEffect(() => {
    if (hovered) {
      clearInterval(timer);
    } else {
      timer = setInterval(changeImage, 2000);
    }
    return () => {
      clearInterval(timer);
    };
  }, [hovered]);

  return (
    <CryptoImageDiv
      src={getImageStringByNumber(imageNumber)}
      onMouseOver={() => setHovered(true)}
      onMouseOut={() => setHovered(false)}
    />
  );
};

export default AnimatedCurrency;
