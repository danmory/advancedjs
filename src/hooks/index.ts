import { useContext } from 'react';
import { RootStore } from '../stores';
import { StoreContext } from '../contexts';

export const useStores = (): RootStore => useContext(StoreContext);
