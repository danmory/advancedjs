import { CoingeckoCoinFormat, RapidapiCoinFormat } from '../types';

export default {
  routes: {
    root: '/javascriptizery',
    selectCrypto: '/javascriptizery/selectCrypto',
    chart: '/javascriptizery/chart',
  },
  api: {
    getPrice: (coin: CoingeckoCoinFormat, days: number) =>
      `https://api.coingecko.com/api/v3/coins/${coin}/market_chart?vs_currency=usd&days=${days}`,
    getNews: (coin: RapidapiCoinFormat) =>
      `https://investing-cryptocurrency-markets.p.rapidapi.com/coins/get-news?pair_ID=${coin}`,
  },
};
