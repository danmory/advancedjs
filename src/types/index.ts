export type Cryptocurrency = 'BTC' | 'ETH' | 'USDT';

export type CoingeckoCoinFormat = 'bitcoin' | 'ethereum' | 'tether';

export type ChartXRange = 'DAY' | 'WEEK' | 'YEAR';

type Timestamp = number;
type USDPrice = number;

export type CoinPrice = [Timestamp, USDPrice];

export type CoinNews = { HEADLINE: string; last_updated: string };

// BTC | ETH | USDT
export type RapidapiCoinFormat = 945629 | 997650 | 1031397;
