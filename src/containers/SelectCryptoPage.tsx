import React, { useState } from 'react';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';
import { Cryptocurrency } from '../types';
import settings from '../settings';
import SelectCryptoModal from '../components/SelectCryptoModal';
import { useStores } from '../hooks';
import AnimatedCurrency from '../components/SelectCryptoAnimatedCurrency';

const StyledSelectCryptoPage = styled.div`
  color: white;
  font-family: 'Inter', sans-serif;
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

const SelectCryptoPage = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const navigate = useNavigate();

  const { cryptoStore } = useStores();

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleCryptoSelect = (cryptocurrency: Cryptocurrency) => {
    setIsModalOpen(false);
    cryptoStore.selectCrypto(cryptocurrency);
    navigate(settings.routes.chart);
  };

  const SelectCryptoContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
  `;

  return (
    <StyledSelectCryptoPage>
      <SelectCryptoContentContainer>
        <AnimatedCurrency />
        <Button variant="outlined" color="inherit" onClick={handleOpenModal}>
          {cryptoStore.selectedCryptocurrency
            ? `Select cryptocurrency: ${cryptoStore.selectedCryptocurrency}`
            : 'Select cryptocurrency'}
        </Button>
      </SelectCryptoContentContainer>
      {isModalOpen && (
        <SelectCryptoModal
          handleClose={handleCloseModal}
          handleCryptoSelect={handleCryptoSelect}
        />
      )}
    </StyledSelectCryptoPage>
  );
};

export default observer(SelectCryptoPage);
