import React, { useEffect } from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  ChartOptions,
  registerables,
} from 'chart.js';
import { Chart } from 'react-chartjs-2';
import { observer } from 'mobx-react';
import LoadingAlert from '../../components/LoadingAlert';
import { ChartXRange } from '../../types';
import { useStores } from '../../hooks';
import {
  StyledChartPage,
  ChartWrapper,
  ChartHeader,
  ChartSection,
} from './ChartPage.styles';
import ChartRangeSelector from '../../components/ChartRangeSelector';
import Rapidapi from '../Rapidapi';
import News from '../../components/News';

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  ...registerables
);

const timestampToDate = (timestamp: number) => {
  const date = new Date(timestamp);
  return `${date.getDate()}/${
    date.getMonth() + 1
  }/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
};

const ChartPage = () => {
  const { cryptoStore } = useStores();
  const { chartXRange, selectedCryptocurrency, prices, news, loadingStatus } =
    cryptoStore;
  const labels = prices.map((p) => timestampToDate(p[0]));
  const price = prices.map((p) => p[1]);
  const handleXRangeChange = (value: ChartXRange) => {
    cryptoStore.setChartXRange(value);
  };
  useEffect(() => {
    cryptoStore.getPrice();
    cryptoStore.getNews();
  }, []);

  const data = {
    labels,
    datasets: [
      {
        type: 'line' as const,
        label: `${selectedCryptocurrency} price`,
        borderColor: '#FF0EE7',
        borderWidth: 1,
        fill: false,
        data: price,
      },
    ],
  };

  const options = {
    elements: { point: { radius: 0, hitRadius: 10 } },
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
    },
  } as ChartOptions;

  return (
    <StyledChartPage>
      {price.length > 0 ? (
        <>
          <ChartSection>
            <ChartHeader>
              {selectedCryptocurrency} price and news by
              <ChartRangeSelector
                handleChange={handleXRangeChange}
                currentValue={chartXRange}
              />
              <Rapidapi />
            </ChartHeader>
            <ChartWrapper>
              <Chart type="bar" data={data} options={options} />
            </ChartWrapper>
          </ChartSection>
          {!!news.length && <News news={news} />}
        </>
      ) : (
        <ChartHeader style={{ height: '100vh' }}>
          No data to display...
        </ChartHeader>
      )}
      {!!loadingStatus && <LoadingAlert>{loadingStatus}</LoadingAlert>}
    </StyledChartPage>
  );
};

export default observer(ChartPage);
