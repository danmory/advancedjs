import styled from '@emotion/styled';

export const StyledChartPage = styled.div`
  user-select: text;
`;

export const ChartSection = styled.section`
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: calc(100vh - 105.33px);
`;

export const ChartWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 80%;
  height: 50%;
  margin: 150px 0;
`;

export const ChartHeader = styled.h2`
  color: white;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
