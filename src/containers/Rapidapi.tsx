import styled from '@emotion/styled';
import React, { useState } from 'react';
import { Button } from '@mui/material';
import { useStores } from '../hooks';
import RapidapiLogo from '../components/RapidapiLogo';
import RapidapiDialog from '../components/RapidapiDialog';

const StyledRapidapi = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  font-weight: 700;
  margin-top: 10px;
  height: 100%;
  span:first-of-type {
    margin-right: 10px;
  }
`;

const Rapidapi: React.FC = () => {
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const { cryptoStore } = useStores();
  return (
    <StyledRapidapi>
      <Button size="small" onClick={() => setDialogOpen(true)} color="inherit">
        Click here to get news from
      </Button>
      <RapidapiLogo />
      <RapidapiDialog
        open={dialogOpen}
        handleClose={() => {
          setDialogOpen(false);
        }}
        handleSubmit={(apiKey: string) => {
          cryptoStore.setApiKey(apiKey);
          setDialogOpen(false);
        }}
      />
    </StyledRapidapi>
  );
};

export default Rapidapi;
