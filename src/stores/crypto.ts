import { runInAction, makeAutoObservable } from 'mobx';
import { getCoinNews, getCoinPrice } from '../services';
import { ChartXRange, CoinNews, CoinPrice, Cryptocurrency } from '../types';

class CryptoStore {
  selectedCryptocurrency: Cryptocurrency = 'BTC';

  chartXRange: ChartXRange = 'YEAR';

  prices: CoinPrice[] = [];

  apiKey = '';

  news: CoinNews[] = [];

  loadingStatus: string = '';

  constructor() {
    makeAutoObservable(this);
  }

  selectCrypto(cryptocurrency: Cryptocurrency) {
    runInAction(() => {
      this.selectedCryptocurrency = cryptocurrency;
      this.getPrice();
    });
  }

  setChartXRange(chartXRange: ChartXRange) {
    runInAction(() => {
      this.chartXRange = chartXRange;
      this.getPrice();
    });
  }

  private getDays() {
    switch (this.chartXRange) {
      case 'DAY':
        return 1;
      case 'WEEK':
        return 7;
      case 'YEAR':
        return 365;
      default:
        return 365;
    }
  }

  async getPrice() {
    runInAction(() => {
      this.loadingStatus = 'Retrieving price...';
    });
    const days = this.getDays();
    const res = await getCoinPrice(this.selectedCryptocurrency, days);
    if (res) {
      runInAction(() => {
        this.prices = res.prices;
      });
    }
    runInAction(() => {
      this.loadingStatus = 'Retrieving is finished...';
    });
    this.removeAlert();
  }

  setApiKey(apiKey: string) {
    runInAction(() => {
      this.apiKey = apiKey;
    });
    this.getNews();
  }

  async getNews() {
    if (this.apiKey) {
      runInAction(() => {
        this.loadingStatus = 'Retrieving news...';
      });
      const res = await getCoinNews(this.selectedCryptocurrency, this.apiKey);
      if (res) {
        runInAction(() => {
          this.loadingStatus = 'News are retrieved!';
          this.news = res?.data[0]?.screen_data?.news || [];
        });
      } else {
        runInAction(() => {
          this.loadingStatus = 'Wrong API key, or service is down!';
        });
      }
      this.removeAlert();
    }
  }

  private removeAlert() {
    setTimeout(
      () =>
        runInAction(() => {
          this.loadingStatus = '';
        }),
      700
    );
  }
}

export default CryptoStore;
