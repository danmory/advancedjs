import { makeAutoObservable } from 'mobx';
import CryptoStore from './crypto';

const cryptoStore = new CryptoStore();

interface IStores {
  cryptoStore;
}

export class RootStore {
  cryptoStore: CryptoStore;

  constructor(stores: IStores) {
    makeAutoObservable(this);
    this.cryptoStore = stores.cryptoStore;
  }
}

const stores = {
  cryptoStore,
};

const rootStore = new RootStore(stores);

export default rootStore;
