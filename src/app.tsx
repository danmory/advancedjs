import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import styled from '@emotion/styled';
import settings from './settings';
import SelectCryptoPage from './containers/SelectCryptoPage';
import General from './components/General/General';
import Header from './components/Header';
import { StoreProvider } from './contexts';
import rootStore from './stores';
import ChartPage from './containers/ChartPage/ChartPage';
import Background from './components/Background';
import BackButton from './components/BackButton';

const GlobalContainer = styled.div<{ isDefaultBackground?: boolean }>`
  user-select: none;
  ${({ isDefaultBackground }) =>
    isDefaultBackground && 'background: rgb(40, 44, 56);'}
`;

const App: React.FC = () => (
  <BrowserRouter>
    <StoreProvider store={rootStore}>
      <Routes>
        <Route
          path={settings.routes.root}
          element={
            <GlobalContainer>
              <Header />
              <General />
              <Background />
            </GlobalContainer>
          }
        />
        <Route
          path={settings.routes.selectCrypto}
          element={
            <GlobalContainer>
              <Header />
              <SelectCryptoPage />
              <Background />
            </GlobalContainer>
          }
        />
        <Route
          path={settings.routes.chart}
          element={
            <GlobalContainer isDefaultBackground>
              <Header>
                <BackButton />
              </Header>
              <ChartPage />
            </GlobalContainer>
          }
        />
      </Routes>
    </StoreProvider>
  </BrowserRouter>
);

export default App;
