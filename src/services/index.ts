import {
  CoingeckoCoinFormat,
  CoinNews,
  CoinPrice,
  Cryptocurrency,
  RapidapiCoinFormat,
} from '../types';
import settings from '../settings';

const convertCoinToAPIFormat = (coin: Cryptocurrency): CoingeckoCoinFormat => {
  switch (coin) {
    case 'BTC':
      return 'bitcoin';
    case 'ETH':
      return 'ethereum';
    case 'USDT':
      return 'tether';
    default:
      return null;
  }
};

const convertCoinToRapidAPIFormat = (
  coin: Cryptocurrency
): RapidapiCoinFormat => {
  switch (coin) {
    case 'BTC':
      return 945629;
    case 'ETH':
      return 997650;
    case 'USDT':
      return 1031397;
    default:
      return null;
  }
};

export const getCoinPrice = async (coin: Cryptocurrency, days: number) => {
  const res = await fetch(
    settings.api.getPrice(convertCoinToAPIFormat(coin), days)
  );
  return res.ok ? (res.json() as Promise<{ prices: CoinPrice[] }>) : null;
};

export const getCoinNews = async (coin: Cryptocurrency, apiKey: string) => {
  const res = await fetch(
    settings.api.getNews(convertCoinToRapidAPIFormat(coin)),
    {
      headers: {
        'X-RapidAPI-Host': 'investing-cryptocurrency-markets.p.rapidapi.com',
        'X-RapidAPI-Key': apiKey,
      },
    }
  );
  return res.ok
    ? (res.json() as Promise<{
        data: { screen_data: { news: CoinNews[] } }[];
      }>)
    : null;
};
