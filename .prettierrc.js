module.exports = {
    semi: true,
    tabWidth: 2,
    singleQuote: true,
    jsxSingleQuote: false,
    trailingComma: 'es5',
    jsxBracketSameLine: false,
    quoteProps: 'as-needed',
    endOfLine: 'auto',
};