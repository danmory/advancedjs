# JavaScriptizery 

Application to watch news and historical changes of cryptocurrencies.

# Requirements

node ^14.19.0

npm ^6.14.0

# Architecture

- src 
  - assets: images and fonts used in app
  - components: React components without connection to MobX store
  - containers: React components with connection to MobX store
  - hooks: custom hooks used in app
  - services: methods for API calls
  - stores: MobX store used in app
  - types: custom types 

# Scripts 

## Run application

<code>npm run start</code>

## Run Unit tests

<code>npm run test</code>

## Run UI tests

<code>npm run cypress</code>

## Build application

<code>npm run build</code>