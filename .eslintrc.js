module.exports = {
  env: {
    browser: true,
    es2021: true,
    jest: true,
  },
  extends: ['plugin:react/recommended', 'airbnb', 'prettier'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'prettier'],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
      },
    },
  },
  rules: {
    'prettier/prettier': 'error',
    'react/jsx-filename-extension': [1, { extensions: ['tsx'] }],
    'react/function-component-definition': [
      1,
      { namedComponents: 'arrow-function' },
    ],
    'import/extensions': 0,
    'no-plusplus': 0,
    'import/prefer-default-export': 0,
    'no-unused-vars': 0,
    'no-shadow': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'no-promise-executor-return': 0,
    'import/no-extraneous-dependencies': 0,
    'no-undef': 0,
    'no-alert': 0,
    'no-nested-ternary': 0,
  },
};
