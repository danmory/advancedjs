describe('App opens', () => {
  it('App works correctly', () => {
    cy.visit('http://localhost:8099');
    cy.get('button').click();
    cy.get('button').click();
    cy.get('input').type('BTC');
    cy.get('button').eq(1).click();
    cy.get('canvas').should('be.visible');
    cy.get('[id=back-btn]').click();
    cy.get('button').click();
    cy.get('input').type('ETH');
    cy.get('button').eq(1).click();
    cy.get('canvas').should('be.visible');
  });
});
