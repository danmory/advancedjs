const pkg = require('./package');

module.exports = {
  apiPath: 'stubs/api',
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
    resolve: {
      extensions: ['.ts', '.js', '.json', 'tsx', 'jsx'],
    },
  },
  navigations: {
    'javascriptizery.main': '/javascriptizery',
    'link.javascriptizery.selectCrypto': '/javascriptizery/selectCrypto',
    'link.javascriptizery.chart': '/javascriptizery/chart',
  },
  features: {
    javascriptizery: {
      // add your features here in the format [featureName]: { value: string }
    },
  },
  config: {
    key: 'value',
  },
};
